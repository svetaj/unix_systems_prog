#include	<stdio.h>
#include	<string.h>
#include 	<sys/types.h>
#include 	<unistd.h>
#include	"mesg.h"
#include	"msgq.h"

int pr50_server (char *, int);

Mesg	mesg;

main()
{
	int	id;

	/* Create the message queue, if required */

	if ( (id = msgget(MKEY1, PERMS | IPC_CREAT)) < 0)
		err_sys("server: can't get message queue 1");

        for (;;)
	    server (id);

	exit (0);
}

server(id)
int id;
{
	int n, buflen;
	char errmesg[256], *sys_err_str();

	/* Read the message from the IPC descriptor */

	mesg.mesg_type = 1L;		/* receive messages of this type */
        memset (mesg.mesg_data, 0, MAXMESGDATA);
	if ( (n = mesg_recv(id, &mesg)) <= 0)
		err_sys ("server: read error");
	mesg.mesg_data[n-sizeof(pid_t)] = '\0';	/* null terminate buffer */

        fprintf (stderr, "Server: received message from %d\n", 
                            mesg.pid_number);

	mesg.mesg_type = (long) mesg.pid_number; /* send m. of this type */

	fprintf (stderr, "Server: BUFFER IN: -->%s<--\n", mesg.mesg_data);

/*----------------------------------------------------------------------*/
/*                            PR50 server                               */
/*----------------------------------------------------------------------*/

        buflen = pr50_server (mesg.mesg_data, n - sizeof(pid_t));

	fprintf (stderr, "Server: posle pr50_server buflen=%d\n", buflen);

/*----------------------------------------------------------------------*/
/*                            PR50 server                               */
/*----------------------------------------------------------------------*/

	mesg.mesg_len = buflen + sizeof(pid_t);
	fprintf (stderr, "Server: mesg.mesg_len=%d\n", mesg.mesg_len);
	mesg_send (id, &mesg);
}


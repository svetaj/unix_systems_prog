/*
				+-----------+
				|  C_PRT.H  |
				+-----------+
+----------------------------------------------------------------------+
|								       |
|		    Last Update :     26 / 11 / 1991		       |
|								       |
+----------------------------------------------------------------------+

*/

#define   PRT_MAX   10

/*
+----------------------------------------------------------------------+
|		    DIAGNOSTICA 				       |
|								       |
|		    REPLAY-CODE 				       |
+----------------------------------------------------------------------+
*/


#define   PRT_OKAY			 700	  /* 0000 */

/*

		    +----------+
		    |  ERROR   |
		    +----------+

*/
#define   PRT_OFF			-700	  /* A7A0 */
#define   PRT_LOCAL			-701	  /* A7A1 */
#define   PRT_READ_ERROR		-702	  /* A7A2 */
#define   PRT_LINE_ERROR		-703	  /* A7A3 */
#define   PRT_BOURRAGE			-704	  /* A7A4 */
#define   PRT_NO_DOCUMENT		-705	  /* A7A5 */
#define   PRT_SPEC_ERROR		-706	  /* A7A6 */
#define   PRT_FAILURE			-707	  /* A7A7 */
#define   PRT_OUT_OF_LINE		-708	  /* A7A8 */
#define   PRT_FREE			-709	  /* A7A9 */
#define   PRT_COMMAND_ERROR		-710	  /* A7AA */
#define   PRT_DOCUMENT_PRESENT		-711	  /* A7AB */
#define   PRT_COMMAND_RESET		-712	  /* A7AC */
#define   PRT_NO_FREE			-713	  /* A7AD */
#define   PRT_REQ_CONF_ERROR		-714	  /* A7AE */
#define   PRT_LOCKED			-715	  /* A7AF */
/* errore dato su pigiata dopo il comando 5D */
#define   PRT_INCOHERENT_STATION	-716	  /* A7AG */
#define   PRT_TRASMISSION_ERROR 	-717	  /* A7AH */
#define   PRT_BUSY			-718	  /* A7AI */



/*

		    +-----------+
		    |  WARNING	|
		    +-----------+

*/
#define   PRT_COMMAND_INCOMPLETE	 701	  /* W7W0 */
#define   PRT_NEAR_END_PAPER		 702	  /* W7W1 */
#define   PRT_NEAR_END_DOCUMENT 	 703	  /* W7W2 */
#define   PRT_DOCUMENT_ALL_IN		 704	  /* W7W3 */
#define   PRT_NEW_STRIPE		 705	  /* W7W4 */
#define   PRT_END_BY_UC 		 706	  /* W7W5 */
#define   PRT_OPEN_CARTER		 707	  /* W7W6 */
#define   PRT_PR_B			 708	  /* W7W7 */

/*
+-------------------------------------+
|				      |
|		    COMMAND	      |
|				      |
+-------------------------------------+
*/

/*
		    +-------------------------------------+
		    |  COMMAND	      CONNECTION HANDLER  |
		    +-------------------------------------+

*/
#define   D_PRT_CONNECT 		  2100	    /* Y000 */
#define   D_PRT_DISCONNECT		  2101	    /* Y001 */
#define   D_PRT_TEST			  2102	    /* Y102 */
#define   D_PRT_WAIT			  2103	    /* Y103 */


/*
		    +----------------------+
		    |  COMMAND SERVER	   |
		    +----------------------+

*/
#define   D_PRT_CONN_PLATE		  2180	    /* S150 */
#define   D_PRT_CONN_AFF		  2181	    /* S251 */
#define   D_PRT_DISC_PR 		  2182	    /* S052 */
#define   D_PRT_TURN_PAGE		  2183	    /* S053 */
#define   D_PRT_WRITE_STRIPE		  2184	    /* S254 */
#define   D_PRT_EJECT			  2185	    /* S055 */
#define   D_PRT_PRINT			  2186	    /* S356 */
#define   D_PRT_IDENTIFY		  2187	    /* S157 */
#define   D_PRT_RESET_CONN_FF		  2188	    /* S058 */
#define   D_PRT_CONN_MFF		  2189	    /* S559 */
#define   D_PRT_READ_STRIPE		  2190	    /* S35A */
#define   D_PRT_READ_OCRB		  2191	    /* S45B */
#define   D_PRT_WRITE_OCRB		  2192	    /* S35C */
#define   D_PRT_CONN_VISA		  2193	    /* S15D */
#define   D_PRT_CONN_IFS		  2194	    /* S15E */
#define   D_PRT_CONN_PUNTO		  2195	    /* S25F */
#define   D_PRT_CONN_MFFP		  2196	    /* S55G */
#define   D_PRT_SHEET_LOAD		  2197	    /* S15H */
#define   D_PRT_LOCK_DEVICE		  2198	    /* S15I */
#define   D_PRT_WAIT_DEVICE		  2201	    /* S15L */
#define   D_PRT_LENGHT_DOC		  2202	    /* S15M */
#define   D_PRT_CHECK_PRINTER		  2203	    /* S05N */
#define   D_PRT_EMUL			  2204	    /* S25O */
#define   D_PRT_PLATE_BUSY		  2205	    /* S15P */
#define   D_PRT_CONN_BADGE		  2206	    /* S25Q */
#define   D_PRT_READ_BADGE		  2207	    /* S45R */
#define   D_PRT_TRASC_PRINT		  2164	    /* S340 */



#define   D_PRT_CONN_PLATE_N		  2280	    /* T150 */
#define   D_PRT_CONN_AFF_N		  2281	    /* T251 */
#define   D_PRT_DISC_PR_N		  2282	    /* T052 */
#define   D_PRT_TURN_PAGE_N		  2283	    /* T053 */
#define   D_PRT_WRITE_STRIPE_N		  2284	    /* T254 */
#define   D_PRT_EJECT_N 		  2285	    /* T055 */
#define   D_PRT_PRINT_N 		  2286	    /* T356 */
#define   D_PRT_IDENTIFY_N		  2287	    /* T157 */
#define   D_PRT_CONN_MFF_N		  2289	    /* T559 */
#define   D_PRT_READ_STRIPE_N		  2290	    /* T35A */
#define   D_PRT_READ_OCRB_N		  2291	    /* T45B */
#define   D_PRT_WRITE_OCRB_N		  2292	    /* T35C */
#define   D_PRT_CONN_VISA_N		  2293	    /* T15D */
#define   D_PRT_CONN_IFS_N		  2294	    /* T15E */
#define   D_PRT_CONN_PUNTO_N		  2295	    /* T25F */
#define   D_PRT_CONN_MFFP_N		  2296	    /* T55G */
#define   D_PRT_SHEET_LOAD_N		  2297	    /* T15H */
#define   D_PRT_LOCK_DEVICE_N		  2298	    /* T15I */
#define   D_PRT_WAIT_DEVICE_N		  2301	    /* T15L */
#define   D_PRT_LENGHT_DOC_N		  2302	    /* T15M */
#define   D_PRT_CHECK_PRINTER_N 	  2303	    /* T05N */
#define   D_PRT_EMUL_N			  2304	    /* T25O */
#define   D_PRT_PLATE_BUSY_N		  2305	    /* T15P */
#define   D_PRT_CONN_BADGE_N		  2306	    /* T25Q */
#define   D_PRT_READ_BADGE_N		  2307	    /* T45R */
#define   D_PRT_TRASC_PRINT_N		  2264	    /* T340 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>
#include "c_prt.h"
#include "prt_ext.h"

#define LINE_CHARACT B4800 | CS8 | CREAD | CLOCAL

int device;
int reply;
char tamo[4096], amo[4096];
char kkontrol[1];
extern int station;
extern int operator;

void println_hex (const char *, unsigned char *, int);

/********************************************************************/
/********************************************************************/

int PRT_connect (char *instance)
{
    fprintf (stderr, "Server: PRT_connect(%s)\n", instance);

    return(0);
}

/*********************************************************************/
/*        funkcija za otvaranje komunikacione linije                 */
/*              i za fizicko kacenje na serijski stampac             */
/*********************************************************************/

int PRT_conn_aff (char *instance, char type_com, int controller, char *param)
{
    struct termio tty_mode;

    char port[] = "/dev/tty00s";

    fprintf (stderr, "Server: PRT_conn_aff(%s,%c,%d,%s)\n",
                     instance, type_com, controller, param);

    device = open (port, O_RDWR);

    if (device == -1)
        return(PRT_OFF);

    if (ioctl (device, TCGETA, &tty_mode) < 0)
        return(PRT_OFF);

    tty_mode.c_iflag = 0;
    tty_mode.c_oflag = 0;
    tty_mode.c_line = 0;
    tty_mode.c_lflag = 0;
    tty_mode.c_cflag = 07655;
    tty_mode.c_cflag = LINE_CHARACT;
    tty_mode.c_cc[VMIN] = 1;
    tty_mode.c_cc[VTIME] = 0;

    if (ioctl (device, TCSETAF, &tty_mode) < 0)
        return(PRT_OFF);

    strcpy(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* +++ DELETED PRINTER ESCAPE SEQUENCE +++ */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Clear fault status                  */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* +++ DELETED PRINTER ESCAPE SEQUENCE +++ */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* DEL DEL                             */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Clear fault status                  */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Clear fault status                  */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* +++ DELETED PRINTER ESCAPE SEQUENCE +++ */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Primary identification request      */
    strcat(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");

    if ((reply = write (device,tamo,strlen(tamo))) == -1)
          return(PRT_OFF);
    do {
        reply = read (device, kkontrol, 1);
    }
    while (kkontrol[0] != 0x1b);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");            /* +++ DELETED PRINTER ESCAPE SEQUENCE +++  */
    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 1);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 1);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");            /* Printer status request        */
    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 9);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");        /* Manuel operator assignment    */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");        /* Clears fault status           */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");    /* Selects Front Feed            */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");        /* Printer status request        */

    reply = write (device, tamo, strlen(tamo));
    do {
        reply = read (device, kkontrol, 1);
    }
    while (kkontrol[0] != 0x1b);
    reply = read (device, amo, 2);
    if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)  /* Prt Sync stat OK */
        return(PRT_OFF);

    if (operator == 1) {
        strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Operator Request LED Control */
                                        /* +++ DELETED PRINTER ESCAPE SEQUENCE +++              */
                                        /* LED ON, Operator 1           */
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Operator Request LED Control */
                                        /* +++ DELETED PRINTER ESCAPE SEQUENCE +++  */
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Printer Status Request       */
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
    }
    else {
        strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Operator Request LED Control */
                                        /* \x52 = 01010010              */
                                        /* LED ON, Operator 2           */
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Operator Request LED Control */
                                        /* \x60 = 11000000  ??????????  */
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Printer Status Request       */
    }

    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 1);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");              /* ??????                       */
    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 1);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");              /* Page size                    */
    reply = write (device, tamo, strlen(tamo));
    reply = read (device, amo, 1);

/*  sa ovim smo dosli do trenutka kada u stampac treba ubaciti papir,  */
/*  te ga sada ubacujemo                                               */

    do {
        reply = read (device, kkontrol, 1);
    }
    while (kkontrol[0] != 0x1b);
    reply = read (device, amo, 2);
    println_hex ("Server: Station 1/2 pressed", (unsigned char *)amo, 2);

    if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) == 0)       /* Printer Reply  */
                                                         /* 00100101       */
                                                         /* Doc present,   */
                                                         /* Stat 1 pressed */
         station = 1;
    else if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) == 0)  /* Printer Reply  */
                                                         /* 00100110       */
                                                         /* Doc present,   */
                                                         /* Stat 2 pressed */
         station = 2;
    else
         return(PRT_OFF);

    strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* ??????????????                 */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Clears fault status            */
    if (station == 1)
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Console assignment             */
                                        /* \x42 = 01000010                */
                                        /* LED OFF Operator 2             */
    else
        strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Console assignment             */
                                        /* \x41 = 01000001                */
                                        /* LED OFF Operator 1             */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Console assignment             */
                                        /* \x44 = 01000100                */
                                        /* ???????                        */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Console assignment             */
                                        /* \x48 = 01001000                */
                                        /* ???????                        */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Selects paper handling device  */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Page size                      */
    strncat (tamo, param, 3);
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Primary identification request */
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Bottom of form (BOF)           */
    strncat (tamo, (param+3), 3);
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Absolute vertical positioning  */
    strncat (tamo, (param+6), 3);
    strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");          /* Printer status request         */

    reply = write (device, tamo, strlen(tamo));
    do {
        reply = read (device, kkontrol, 1);
    }
    while (kkontrol[0] != 0x1b);
    reply = read (device, amo, 2);
    if ((reply = strncmp(amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)   /* Prt Sync stat OK */
         return(PRT_OFF);
    return(PRT_OKAY);
}




/**************************************************************************/
/*          funkcija za stampanje                                         */
/**************************************************************************/

int PRT_print (char *instance, char type_com, int controller,
               char *buffer, int lbuffer)
{
      fprintf (stderr, "Server: PRT_print(%s,%c,%d,%s,%d)\n",
                       instance, type_com, controller, buffer, lbuffer);

      strcpy (tamo, "\x1b\x53\x35");   /* Select paper handling device */
      strcat (tamo, buffer);           /* data                         */
      strcat (tamo, "\x0d");           /* CR                           */
      strcat (tamo, "\x1b\x6a");       /* Printer status request       */

      reply = write (device, tamo, strlen(tamo));
      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);
      reply = read (device, amo, 2);
      if ((reply = strncmp (amo, "\x72\x50",2)) != 0)  /* Prt Sync stat OK */
            return(PRT_OFF);
      return(PRT_OKAY);
}



/**************************************************************************/
/*         funkcija za izbacivanje papira iz stampaca                     */
/**************************************************************************/

int PRT_eject (char *instance, char type_com)
{
     fprintf (stderr, "Server: PRT_eject(%s,%c)\n", instance, type_com);

     strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");  /* Selects paper handling device */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Paper expulsion               */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");      /* Printer status request        */

     reply = write (device, tamo, strlen(tamo));
     do {
         reply = read (device, kkontrol, 1);
     }
     while (kkontrol[0] != 0x1b);
     reply = read (device, amo, 2);

     if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0) /* Prt Sync stat OK */
          return(PRT_OFF);
     return(PRT_OKAY);
}


/**************************************************************************/
/*        funkcija za fizicko otkacinjanje od stampaca                    */
/*              i za zatvaranje komunikacione linije                      */
/**************************************************************************/

int PRT_disc_pr (char *instance, char type_com)
{
     fprintf (stderr, "Server: PRT_disc_pr(%s,%c)\n", instance, type_com);

     strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");         /* Manual operator asignment   */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");         /* Clears fault status         */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");     /* ??????????????              */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");         /* Clears fault status         */
     if (operator == 1)
         strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++"); /* Console asignment           */
                                        /* \x41 = 10000001             */
                                        /* Switches off led 1          */
     else
         strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++"); /* Console asignment           */
                                        /* \x42 = 10000010             */
                                        /* Switches off led 2          */
     strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");         /* Clears fault status         */

     reply = write (device, tamo, strlen(tamo));

     close(device);

     return(PRT_OKAY);
}



/**************************************************************************/
/*                Raskidanje veze sa serverom                             */
/**************************************************************************/

int PRT_disconnect (char *instance)
{
    fprintf (stderr, "Server: PRT_disconnect(%s)\n", instance);

    return (0);
}



/**************************************************************************/
/*                    Stampa buffer u hex formatu                         */
/**************************************************************************/

void println_hex (const char *text, unsigned char *answ, int max)
{
     int i;

     if (max > 0)
         fprintf(stderr, "%s\t: hex value : |", text);

     for (i=0; i < max; i++)
         fprintf(stderr, " %x |", answ[i] );

     if (max > 0)
         fprintf(stderr, "\n");
}

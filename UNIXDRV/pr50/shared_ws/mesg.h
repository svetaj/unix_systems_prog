#define MAXMESGDATA	(4096-16)

#define	MESGHDRSIZE	(sizeof(Mesg) - MAXMESGDATA)

typedef struct {
	int	mesg_len;	/* #bytes in mesg_data, can be 0 or > 0 */
	long	mesg_type;	/* message type, must be > 0 */
        pid_t   pid_number;     /* pid number of caller   */
	char	mesg_data [MAXMESGDATA];
}	Mesg;

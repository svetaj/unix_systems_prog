/*==================================================================*/
/*   Input buffer                                                   */
/*                                                                  */
/*                       1                   2                   3  */
/*   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0  */
/*  |C|O|N|N|C|1|                                                   */
/*  |D|I|S|C|C|1|                                                   */
/*  |E|J|E|C|C|1|S|                                                 */
/*  |D|S|C|P|C|1|S|                                                 */
/*  |C|A|F|F|C|1|S|  mode |0|8|0|9|9|9|0|0|2|                       */
/*  |P|R|N|T|C|1|S|  mode | nchar | data to be printed   ...    |   */
/*   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0  */
/*                       1                   2                   3  */
/*                                                                  */
/*   Output buffer                                                  */
/*                                                                  */
/*   0 1 2 3                                                        */
/*  | reply |                                                       */
/*==================================================================*/

#include <stdio.h>
#include <string.h>
#include "c_prt.h"
#include "prt_ext.h"

int station;
int operator;
char buf4[5];

int pr50_server (char *buffer, int buflen)
{
     char prt_inst[3] = "C1\0";
     unsigned char param[10] = "080999002\0";
     int *mode, mode_value;
     char susp = 'S';
     int *nochar, nochar_value;
     int reply;

     fprintf(stderr,"Server: pre inicijalizacije\n");
     mode_value = 7;
     mode = &mode_value;
     nochar_value = 0;
     nochar = &nochar_value;
     prt_inst[0] = buffer[4];
     prt_inst[1] = buffer[5];
     prt_inst[2] = '\0';
     reply = PRT_OFF;
     fprintf(stderr,"Server: posle inicijalizacije\n");
     
     if (strncmp(buffer, "CONN", 4) == 0) {
           if (buflen == 7) {
               if (buffer[6] == '1')
                   operator = 1;
               else if (buffer[6] == '2')
                   operator = 2;
               else
                   operator = 1;
               fprintf (stderr, "Server: PRT_connect operator=%d\n", operator);
               reply = PRT_connect (prt_inst);
               fprintf (stderr, "Server: PRT_connect %d\n",reply);
           }
     }
     else if (strncmp(buffer, "DISC", 4) == 0) {
           if (buflen == 6) {
               reply = PRT_disconnect (prt_inst);
               fprintf (stderr, "Server: PRT_disconnect %d\n",reply);
           }
     }
     else if (strncmp(buffer, "CAFF", 4) == 0) {
           if (buflen == 20) {
               fprintf(stderr,"Server: izabran CAFF\n");
               susp = buffer[6];
               buf4[0] = buffer[7];
               buf4[1] = buffer[8];
               buf4[2] = buffer[9];
               buf4[3] = buffer[10];
               buf4[4] = '\0';
               sscanf (buf4, "%d", mode);
               strncpy ((char *)param, (const char *)buffer+11, (size_t) 9);
               param[9] = '\0';
               fprintf(stderr,"Server: pre PRT_conn_aff\n");
               reply = PRT_conn_aff (prt_inst, susp, *mode, (char *)param);
               fprintf(stderr,"Server: PRT_conn_aff %d\n",reply);
           }
     }
     else if (strncmp(buffer, "EJEC", 4) == 0) {
           if (buflen == 7) {
               susp = buffer[6];
               reply = PRT_eject (prt_inst, susp);
               fprintf (stderr,"Server: PRT_eject %d\n",reply);
           }
     }
     else if (strncmp(buffer, "DSCP", 4) == 0) {
           if (buflen == 7) {
               susp = buffer[6];
               reply = PRT_disc_pr (prt_inst, susp);         
               fprintf (stderr, "Server: PRT_disc_pr %d\n", reply); 
           }
     }
     else if (strncmp(buffer, "PRNT", 4) == 0) {
           if (buflen >= 16) {
               susp = buffer[6];
               buf4[0] = buffer[7];
               buf4[1] = buffer[8];
               buf4[2] = buffer[9];
               buf4[3] = buffer[10];
               buf4[4] = '\0';
               sscanf (buf4, "%d", mode);
               buf4[0] = buffer[11];
               buf4[1] = buffer[12];
               buf4[2] = buffer[13];
               buf4[3] = buffer[14];
               buf4[4] = '\0';
               sscanf (buf4, "%d", nochar);
               reply = PRT_print (prt_inst, susp, *mode, buffer+15, *nochar);
               fprintf (stderr, "Server: PRT_print %d\n",reply); 
           }
     }
     else
           reply = PRT_OFF;

     sprintf (buffer, "%d\0", reply);
     fprintf (stderr, "Server: buflen out =%d\n", strlen(buffer)); 
     return (strlen (buffer));
}

/*==================================================================*/
/*   Output buffer                                                  */
/*                                                                  */
/*                       1                   2                   3  */
/*   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0  */
/*  |C|O|N|N|C|1|x|                                                 */
/*  |D|I|S|C|C|1|                                                   */
/*  |E|J|E|C|C|1|S|                                                 */
/*  |D|S|C|P|C|1|S|                                                 */
/*  |C|A|F|F|C|1|S|  mode |0|8|0|9|9|9|0|0|2|                       */
/*  |P|R|N|T|C|1|S|  mode | nchar | data to be printed   ...    |   */
/*   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0  */
/*                       1                   2                   3  */
/*                                                                  */
/*  ( x - Operator [1,2] )                                          */
/*                                                                  */
/*   Input buffer                                                   */
/*                                                                  */
/*   0 1 2 3                                                        */
/*  | reply |                                                       */
/*==================================================================*/


#include	<stdio.h>
#include	<sys/types.h>
#include	<unistd.h>
#include	<string.h>
#include	"mesg.h"
#include	"msgq.h"
#include 	"c_prt.h"
#include 	"prt_ext.h"

Mesg	mesg;
int client (int id, char *buffer, int buflen);

int reply;
char buffer[MAXMESGDATA];
char buf4[5];
int buflen;
extern int operator;

int id;


int PRT_connect (char *prt_inst) 
{
    fprintf (stderr, "Client: PRT_connect(%s)\n", prt_inst);

   /* Open a single message queue. The server must have already created */

   if ( (id = msgget(MKEY1, 0)) < 0) {
	err_sys("Client: can't get message queue 1");
        return (-1);
   }
   buffer[0] = 'C';
   buffer[1] = 'O';
   buffer[2] = 'N';
   buffer[3] = 'N';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   if (operator == 1)
       buffer[6] = '1';
   else
       buffer[6] = '2';
   buflen = 7;
   if ( client (id, buffer, buflen) != 0)
       return(PRT_OFF);
   fprintf (stderr, "Client: buffer from server -->%x%x%x%x<--\n", 
                    buffer[0], buffer[1], buffer[2], buffer[3]);
   sscanf (buffer, "%d", &reply);
   fprintf (stderr, "Client: posle sscanf");
   return (reply);
}


int PRT_disconnect (char *prt_inst)
{
   fprintf (stderr, "Client: PRT_disconnect(%s)\n", prt_inst);

   buffer[0] = 'D';
   buffer[1] = 'I';
   buffer[2] = 'S';
   buffer[3] = 'C';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   return (0);
}


int  PRT_conn_aff (char *prt_inst, char susp, int mode, char *param)
{
   fprintf (stderr, "Client: PRT_conn_aff(%s,%c,%d,%s)\n", 
                     prt_inst, susp, mode, param);

   buffer[0] = 'C';
   buffer[1] = 'A';
   buffer[2] = 'F';
   buffer[3] = 'F';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   buffer[6] = susp;
   sprintf (buf4, "%4.4s", mode);
   buffer[7] = buf4[0];
   buffer[8] = buf4[1];
   buffer[9] = buf4[2];
   buffer[10] = buf4[3];
   memcpy (buffer+11, param, 9);
   buflen = 20;
   if ( client (id, buffer, buflen) != 0)
       return(PRT_OFF);
   sscanf (buffer, "%d", &reply);
   return (reply);
}
      
int  PRT_disc_pr (char *prt_inst, char susp)
{
   fprintf (stderr, "Client: PRT_disc_pr(%s,%c)\n", prt_inst, susp);

   buffer[0] = 'D';
   buffer[1] = 'S';
   buffer[2] = 'C';
   buffer[3] = 'P';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   buffer[6] = susp;
   buflen = 7;
   if ( client (id, buffer, buflen) != 0)
       return(PRT_OFF);
   sscanf (buffer, "%d", &reply);
   return (reply);
}


int  PRT_eject (char *prt_inst, char susp)
{
   fprintf (stderr, "Client: PRT_eject(%s,%c)\n", prt_inst, susp);

   buffer[0] = 'E';
   buffer[1] = 'J';
   buffer[2] = 'E';
   buffer[3] = 'C';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   buffer[6] = susp;
   buflen = 7;
   if ( client (id, buffer, buflen) != 0)
       return(PRT_OFF);
   sscanf (buffer, "%d", &reply);
   return (reply);
}


int  PRT_print (char *prt_inst, char susp, int mode, char *line, int length)
{
   fprintf (stderr, "Client: PRT_print(%s,%c,%d,%s,%d)\n", 
                     prt_inst, susp, mode, line, length);

   buffer[0] = 'P';
   buffer[1] = 'R';
   buffer[2] = 'N';
   buffer[3] = 'T';
   buffer[4] = prt_inst[0];
   buffer[5] = prt_inst[1];
   buffer[6] = susp;
   sprintf (buf4, "%4.4s", mode);
   buffer[7] = buf4[0];
   buffer[8] = buf4[1];
   buffer[9] = buf4[2];
   buffer[10] = buf4[3];
   sprintf (buf4, "%4.4s", length);
   buffer[11] = buf4[0];
   buffer[12] = buf4[1];
   buffer[13] = buf4[2];
   buffer[14] = buf4[3];
   memcpy (buffer+15, line, length);
   buflen = 15 + length;
   if ( client (id, buffer, buflen) != 0)
       return(PRT_OFF);
   sscanf (buffer, "%d", &reply);
   return (reply);
}


int client (int id, char *buffer, int buflen)
{
	int n;

      /* write message to the IPC descriptor */

        memset ((void *)mesg.mesg_data, 0, MAXMESGDATA);
        memcpy ((void *)mesg.mesg_data, (const void *)buffer, (size_t)buflen);

	mesg.mesg_len = buflen+sizeof(pid_t);
	mesg.mesg_type = 1L;		/* send messages of this type */
        mesg.pid_number = getpid();

	if (mesg_send (id, &mesg) < 0) {
	      err_sys("data write error from IPC");
              return(-1);
        }

	/* Receive the message from the IPC descriptor */

	mesg.mesg_type = (long) getpid();  /* receive messages of this type */

	if ((n = mesg_recv(id, &mesg)) > 0) {
             fprintf (stderr, "Client: posle mesg_recv n=%d\n", n);
             buflen = n - sizeof (pid_t);
             memset ((void *)buffer, 0, MAXMESGDATA);
             memcpy ((void *)buffer, (const void *)mesg.mesg_data, 
                                     (size_t) buflen);
             buffer[buflen] = '\0';
             fprintf (stderr, "Client: posle memcpy\n");
             return(0);
        }
	else {
	     err_sys("data read error from IPC");
             return(-1);
        }
}

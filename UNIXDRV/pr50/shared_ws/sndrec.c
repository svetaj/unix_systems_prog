#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include "mesg.h"
#include "msgq.h"

/* Send a message using the System V message queues.
   The mesg_len, mesg_type and mesg_data fields must be filled
   in by the caller */

mesg_send (id, mesgptr)
int     id;             /* really an msgid from msgget() */
Mesg    *mesgptr;
{
        /* Send the message - the type followed by the optional data */

	fprintf (stderr, "\t\t\tmsgsnd pre PID = %d\n", getpid());
        if ( msgsnd (id, (char *) &(mesgptr->mesg_type),
                                        mesgptr->mesg_len, 0) != 0)
                err_sys("msgsnd error");
	fprintf (stderr, "\t\t\tmsgsnd posle PID = %d\n", getpid());
}



/* Receive a message from a System V message queue,
   The caller must fill in the mesg_type field with the desired type.
   Return the number of bytes in the data portion of the message.
   A 0-length data message implies EOF */


mesg_recv (id, mesgptr)
int     id;             /* really an msgid from msgget() */
Mesg    *mesgptr;
{
	int	n;

        /* Read the first message on the queue of the specified type */

	fprintf (stderr, "\t\t\tmsgrcv pre PID = %d\n", getpid());
        n = msgrcv (id, (char *) &(mesgptr->mesg_type), MAXMESGDATA,
                                        mesgptr->mesg_type, 0);
	fprintf (stderr, "\t\t\tmsgrcv posle PID = %d\n", getpid());

	if ( (mesgptr->mesg_len = n) < 0)
                err_sys("msgrcv error");

        return(n);              /* n will be 0 at end of file */
}

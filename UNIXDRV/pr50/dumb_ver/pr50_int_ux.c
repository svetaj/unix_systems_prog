#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termio.h>

#define LINE_CHARACT B4800 | CS8 | CREAD | CLOCAL

int device, lock_device;
int reply;
char tamo[2048],amo[2048];
char kkontrol[1];

#define LOCK_FILE "/tmp/PR50LOCK"

/********************************************************************/
/*        funkcija za otvaranje komunikacione linije                */
/********************************************************************/


int PRT_connect (char *instance)
{
    struct termio tty_mode;

    char port[] = "/dev/tty00s";

    device = open (port, O_RDWR);

    if (device == -1)
        return(-700);

    lock_device = open (LOCK_FILE, O_CREAT | O_RDWR);
    my_lock (lock_device);

    if (ioctl (device, TCGETA, &tty_mode) < 0)
        return(-700);

    tty_mode.c_iflag = 0;
    tty_mode.c_oflag = 0;
    tty_mode.c_line = 0;
    tty_mode.c_lflag = 0;
    tty_mode.c_cflag = 07655;
    tty_mode.c_cflag = LINE_CHARACT;
    tty_mode.c_cc[VMIN] = 1;
    tty_mode.c_cc[VTIME] = 0;

    if (ioctl (device, TCSETAF, &tty_mode) < 0)
        return(-700);

    return(0);
}

/*********************************************************************/
/*       funkcija za fizicko kacenje na serijski stampac             */
/*********************************************************************/

int PRT_conn_aff (char *instance, char type_com, int controller, char *param)
{

      strcpy(tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      if ((reply = write (device,tamo,strlen(tamo))) == -1)
            return(-700);
      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 1);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 1);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 9);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);

      reply = read (device, amo, 2);
      if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)
            return(-700);

      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 1);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 1);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      reply = read (device, amo, 1);

/*  sa ovim smo dosli do trenutka kada u stampac treba ubaciti papir,  */
/*  te ga sada ubacujemo                                               */

      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);
      reply = read (device, amo, 2);
      if ((reply = strncmp (amo,"\x72\x25",2)) != 0)
           return(-700);
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      strncat (tamo, param, 3);
      strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      strncat (tamo, (param+3), 3);
      strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      strncat (tamo, (param+6), 3);
      strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);
      reply = read (device, amo, 2);
      if ((reply = strncmp(amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)
           return(-700);
      return(700);
}




/**************************************************************************/
/*          funkcija za stampanje                                         */
/**************************************************************************/

int PRT_print (char *instance, char type_com, int controller,
               char *buffer, int lbuffer)
{
      strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      strcat (tamo, buffer);
      strcat (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
      reply = write (device, tamo, strlen(tamo));
      do {
          reply = read (device, kkontrol, 1);
      }
      while (kkontrol[0] != 0x1b);
      reply = read (device, amo, 2);
      if ((reply = strncmp (amo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)
            return(-700);
      return(700);
}



/**************************************************************************/
/*         funkcija za izbacivanje papira iz stampaca                     */
/**************************************************************************/

int PRT_eject (char *instance, char type_com)
{

     strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
     reply = write (device, tamo, strlen(tamo));
     do {
         reply = read (device, kkontrol, 1);
     }
     while (kkontrol[0] != 0x1b);
     reply = read (device, amo, 2);
     if ((reply = strncmp (amo,"+++ DELETED PRINTER ESCAPE SEQUENCE +++",2)) != 0)
          return(-700);
     return(700);
}


/**************************************************************************/
/*        funkcija za fizicko otkacinjanje od stampaca                    */
/**************************************************************************/

int PRT_disc_pr (char *instance, char type_com)
{
     strcpy (tamo, "+++ DELETED PRINTER ESCAPE SEQUENCE +++");
     reply = write (device, tamo, strlen(tamo));
     return(700);
}



/**************************************************************************/
/*       funkcija za zatvaranje komunikacione linije                      */
/**************************************************************************/

int PRT_disconnect (char *instance)
{
    my_unlock(lock_device);
    close(lock_device);

    return (close(device));
}

#define LCK_MODE_SV	        /* System V lock*/
/*#define LCK_MODE_BSD*/        /* 4.3BSD lock 	*/
/*#define LCK_MODE_LINK*/	/* using link 	*/
/*#define LCK_MODE_CREAT*/   	/* using creat 	*/
/*#define LCK_MODE_OPEN*/	/* using open 	*/
/*#define LCK_MODE_SEM*/    	/* using semaphores */
                                
#include       <unistd.h>
#include	<fcntl.h>



#ifdef LCK_MODE_SV

/* Locking routines for System V (MANDATORY) */
/* lock is in the standard C library, calls fcntl system call */


my_lock (fd)
int     fd;
{
        lseek (fd, 0L, 0);                      /* rewind before lockf */
        if (lockf(fd, F_LOCK, 0L) == -1)        /* 0L -> lock entire file */
                err_sys ("can't F_LOCK");
        return;
}

my_unlock (fd)
int     fd;
{
        lseek (fd, 0L, 0);
        if (lockf (fd, F_ULOCK, 0L) == -1)
                err_sys ("can't F_ULOCK");
        return;
}

#endif

#ifdef LCK_MODE_BSD

/* Locking routines for 4.3BSD (ADVISORY) */

#include <sys/file.h>

my_lock (fd)
int     fd;
{
        if (Flock(fd, LOCK_EX) == -1)     
                err_sys ("can't LOCK_EX");
        return;
}

my_unlock (fd)
int     fd;
{
        if (flock (fd, LOCK_UN, 0L) == -1)
                err_sys ("can't LOCK_UN");
        return;
}

#endif


#ifdef LCK_MODE_LINK

/* Locking routines using the link() system call */

#define LOCKFILE "seqno.lock"

#include	<sys/errno.h>
extern int	errno;

my_lock(fd)
int	fd;
{
	int	tempfd;
	char	tempfile[30];

	sprintf (tempfile, "LCK%d", getpid());

	/* create a temporary file,then close it.
	   if the temporary file aleready exists, the creat() will
	   just truncate it to 0-lenght */

	if ( (tempfd = creat (tempfile, 0444)) < 0)
		err_sys("can't creat temp file");
	close(tempfd);

	/* Now try to rename the temporary file to the lock file.
	   This will fail if the lock file already exists (i.e., if 
	   some other process has a lock */

	while (link(tempfile, LOCKFILE) < 0) {
		if (errno != EEXIST)
			err_sys ("link error");
		sleep(1);
	}
	if (unlink (tempfile) < 0)
		err_sys ("unlink error for tempfile");
}

my_unlock(fd)
int fd;
{
	if (unlink(LOCKFILE) < 0)
		err_sys ("unlink error for LOCKFILE");
}

#endif


#ifdef LCK_MODE_CREAT


/* Locking routines using a creat() system call with all permissions
   turned off */

#include	<sys/errno.h>
extern int	errno;

#define LOCKFILE	"seqno.lock"
#define TEMPLOCK	"temp.lock"

my_lock(fd)
int	fd;
{
	int	tempfd;

	/* Try to create a temporary file, with all write permissions
	   turned off. If the temporary file already exists, the creat()
	   will fail. */

	while ( (tempfd = creat (TEMPLOCK, 0)) < 0) {
		if (errno != EACCES)
			err_sys ("creat error");
		sleep(1);
	}
	close(tempfd);
}

my_unlock(fd)
int	fd;
{
	if (unlink(TEMPLOCK) < 0)
		err_sys ("unlink error for tempfile");
}

#endif



#ifdef LCK_MODE_OPEN

/* Locking routines using a open() system call with both O_CREAT
   and O_EXCL specified */


#include	<sys/errno.h>
extern int	errno;

#define LOCKFILE	"seqno.lock"
#define PERMS		0666	

my_lock(fd)
int	fd;
{
	int	tempfd;

	/* Try to create the lock file, using open() with both
	   O_CREAT and O_EXCL */

	while ( (tempfd = open (LOCKFILE, O_RDWR|O_CREAT|O_EXCL, PERMS)) < 0) {
		if (errno != EEXIST)
			err_sys ("open error for lock file");
		sleep(1);
	}
	close(tempfd);
}

my_unlock(fd)
int	fd;
{
	if (unlink(LOCKFILE) < 0)
		err_sys ("unlink error for lockfile");
}

#endif

err_sys(msg)
char *msg;
{
     printf("error: %s\n",msg);
}

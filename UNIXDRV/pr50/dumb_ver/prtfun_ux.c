/*---------------------------------------------------------------------------*/
/*  Glavni program: stampa fajl preko PRT servisa.                           */
/*                  Taraba (#) znaci novu stranu                             */
/*---------------------------------------------------------------------------*/
/*                       (C) SVETA     1992, 1993, 1994                      */
/*---------------------------------------------------------------------------*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <float.h>
#include <malloc.h>
#include <math.h>
#include <memory.h>
#include <search.h>
#include <signal.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include "prt_ext.h"
#include "c_prt.h"

int loaded_paper=0;
int fd,fdout,tm_flag;
int pool_time = 30;
char prt_inst_gl[3];
FILE *fpizv;

#define NEW_PAGE_CHARACTER ''
#define MAX_LINE_READ 1024
#define OK PRT_OKAY
#define TIME_OUT_ERR	-2
#define TIME_INIT        5
#define TIME_EJECT       5
#define TIME_PRINT       5
#define TIME_CONN        5

void sig_alarm();
void sig_handler();


main (int argc, char *argv[])
{
    int reply, i, status;
    char pom_line[MAX_LINE_READ+1], prt_inst[3];
    time_t *now;
    FILE *fptr;
 
/*----------------------------------------------------------------*/
/*  Ucitavanje parametara iz komandne linije                      */
/*----------------------------------------------------------------*/

    if( argc < 3)
        exit(0);

    if(*argv[3] != 'D') {
	fd=open("/dev/null",O_WRONLY);
	close(2);
	fdout=dup(fd);
	close(fd);
    }

/*----------------------------------------------------------------*/
/*  Setovanje interrupt handlera                                  */
/*----------------------------------------------------------------*/

    signal (SIGHUP, (*sig_handler));
    signal (SIGINT, (*sig_handler));
    signal (SIGQUIT, SIG_IGN);
    signal (SIGTERM, (*sig_handler));

    sig_alarm(0);
    tm_flag=0;
    alarm(0);

/*----------------------------------------------------------------*/
/*  Ako fajl ne postoji - mars napolje iz programa                */
/*----------------------------------------------------------------*/

    if (access (argv[1], 4))
        exit(1);


/*----------------------------------------------------------------*/
/*  Konektovanje na servis                                        */
/*----------------------------------------------------------------*/

    strcpy(prt_inst,argv[2]);
    strcpy(prt_inst_gl,argv[2]);

    reply = connect_to_PRT_service (prt_inst) ;
    if ( reply == 0 || reply == 1 ) {
  
/*----------------------------------------------------------------*/
/*  Otvaranje fajla                                               */
/*----------------------------------------------------------------*/

       if ( (fpizv = fopen (argv[1],"r")) != NULL) {
      
/*----------------------------------------------------------------*/
/*  Citanje po jedne linije iz fajla i stampanje                  */ 
/*----------------------------------------------------------------*/

          while ( fgets(pom_line,MAX_LINE_READ,fpizv) != NULL ) {
               if (pom_line[0] != NEW_PAGE_CHARACTER) {
                    if(loaded_paper == 0) {    
                        if (load_paper_into_printer (prt_inst) != OK)
                            break;
                        if (initialize_printer (prt_inst) != OK)
                            break;
                        loaded_paper = 1;
                    }
                    if (print_one_line (prt_inst, pom_line) != OK)
                            break;
               }
               else {
                    if (eject_paper (prt_inst) != OK)
                        break;
                    if (disconnect_from_printer (prt_inst) != OK)
                        break;
                    loaded_paper = 0;
                    if (pom_line[1] == NEW_PAGE_CHARACTER) 
                        break;
               }
           }
       }
 
/*----------------------------------------------------------------*/
/*  Kraj rada i zatvaranje fajla                                  */
/*----------------------------------------------------------------*/

        if(loaded_paper == 1) {
            eject_paper (prt_inst);
            disconnect_from_printer (prt_inst);
        }
        disconnect_from_PRT_service (prt_inst);
        fclose (fpizv);                           
    }
    exit(0);
}

/*----------------------------------------------------------------*/
/*  Podprogram: Konektovanje na PRT servis                        */
/*----------------------------------------------------------------*/

int connect_to_PRT_service(char *prt_inst)
{
    int reply;

    reply = PRT_connect (prt_inst);
    fprintf (stderr, "PRT_connect %d\n",reply);
    return(reply);
}

/*----------------------------------------------------------------*/
/*  Podprogram: Uvlacenje papira u printer                        */
/*----------------------------------------------------------------*/

int load_paper_into_printer(char *prt_inst)
{
    unsigned char param[10] = "080999002\0";
    int reply;

    printf("INSERT DOCUMENT\n");

    reply = PRT_conn_aff (prt_inst, 'S', 7, param);
    fprintf(stderr,"PRT_conn_aff %d\n",reply);

    return(reply);                      
}

/*----------------------------------------------------------------*/
/*  Podprogram: Inicijalizacija printera                          */
/*----------------------------------------------------------------*/

int initialize_printer(char *prt_inst)
{
    int reply;

    alarm(pool_time+TIME_INIT);
    reply = PRT_print (prt_inst, 'S', 7, "\x1B\x3C\0", 2);
    alarm(0);
    if(tm_flag==1) {
	reply=TIME_OUT_ERR;
        tm_flag=0;
    }
    fprintf (stderr,"PRT_print %d\n", reply);
    return(reply);
}

/*----------------------------------------------------------------*/
/*  Podprogram: Izbacivanje papira                                */
/*----------------------------------------------------------------*/

int eject_paper(char *prt_inst)
{
    int reply;

    alarm(pool_time+TIME_EJECT);
    reply = PRT_eject (prt_inst, 'S');
    alarm(0);
    if(tm_flag==1) {
	reply=TIME_OUT_ERR;
        tm_flag=0;
    }
    fprintf (stderr,"PRT_eject %d\n",reply);
 
    return(reply);
}

/*----------------------------------------------------------------*/
/*  Podprogram: Stampa jednu liniju                               */
/*----------------------------------------------------------------*/

int print_one_line (char *prt_inst, char *pom_line)
{
    int reply;

    alarm(pool_time+TIME_PRINT);
    reply = PRT_print (prt_inst, 'S', 7, pom_line, strlen(pom_line));
    alarm(0);
    if(tm_flag==1) {
	reply=TIME_OUT_ERR;
        tm_flag=0;
    }
    fprintf (stderr, "PRT_print %d\n",reply); 
    return(reply);
} 

/*----------------------------------------------------------------*/
/*  Podprogram: Disconnect od printera                            */
/*----------------------------------------------------------------*/

disconnect_from_printer (char *prt_inst)
{
    int reply;

    reply = PRT_disc_pr (prt_inst, 'S');
    fprintf (stderr, "PRT_disc_pr %d\n",reply);
     
    return(reply);
}

/*----------------------------------------------------------------*/
/*  Podprogram: Disconnect od PRT servisa                         */
/*----------------------------------------------------------------*/

int disconnect_from_PRT_service (char *prt_inst)
{
    int reply;

    reply = PRT_disconnect (prt_inst);         
    fprintf (stderr, "PRT_disconnect %d\n", reply); 

    return(reply);
}


/*----------------------------------------------------------------*/
/*  Interrupt handler - aktivira vestacki kraj programa           */
/*----------------------------------------------------------------*/

void sig_handler(signo)
int	signo;
{

	fprintf(stderr,"Signal handler: %d\n",signo); 
        if(loaded_paper == 1) {
            eject_paper (prt_inst_gl);
            disconnect_from_printer (prt_inst_gl);
        }
        disconnect_from_PRT_service (prt_inst_gl);
	fprintf(stderr,"Signal handler: exit\n"); 
	exit(1);
}

/*---------------------------------------------------------------*/
/* Interrupt handler  - setuje flag da je isteklo vreme          */
/*---------------------------------------------------------------*/

void sig_alarm(signo)
int	signo;
{
       fprintf(stderr,"Signal alarm : %d\n",signo); 
       signal(SIGALRM,(*sig_alarm));
       tm_flag=1;
}

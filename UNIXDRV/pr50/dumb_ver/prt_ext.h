/*	 COPYRIGHT (C) BY ING. C. OLIVETTI & C.,SPA, 1988,1992		      */
/*	 parametri funzioni PRT per applicativi "c"  22-Mag-1992              */
/*----------------------------------------------------------------------------*/

extern	 int  PRT_connect(char *);
extern	 int  PRT_disconnect(char *);
extern	 int  PRT_wait(char *, int *);
extern	 int  PRT_test(char *, int *);
extern	 int  PRT_reply(char *, int);
extern	 int  PRT_identify(char *, char , char*);
extern	 int  PRT_lldoc(char *, char , char*);
extern	 int  PRT_lock_device(char *, char , int);
extern	 int  PRT_wait_device(char *, char , int);
extern	 int  PRT_conn_plate(char *, char , int);
extern	 int  PRT_conn_ifs(char *, char , int);
extern	 int  PRT_conn_aff(char *, char , int , char *);
extern	 int  PRT_conn_punto(char *, char , int , char *);
extern	 int  PRT_conn_mff(char *, char , int , int *, char *, char *, int);
extern	 int  PRT_conn_mpunto(char *, char , int , int *, char *, char *, int);
extern	 int  PRT_disc_pr(char *, char );
extern	 int  PRT_eject(char *, char );
extern	 int  PRT_turn_page(char *, char );
extern	 int  PRT_reset_conn_ff(char *);
extern	 int  PRT_sheet_load(char *, char , int );
extern	 int  PRT_print(char *, char , int , char *, int );
extern	 int  PRT_read_ocrb(char *, char , int *, char *, char *, int);
extern	 int  PRT_read_stripe(char *, char , int *, char *, int);
extern	 int  PRT_write_ocrb(char *, char , char *, char *, int);
extern	 int  PRT_write_stripe(char *, char , char *, int);
extern	 int  PRT_plate_busy(char *, char , int);
extern	 int  PRT_conn_badge(char *, char , int , int);
extern	 int  PRT_read_badge (char *, char , int , int * , char *, int);
extern	 int  PRT_trasc_print(char *, char , int , char *, int );
